// ---Object Literal---

/* 
let mahasiswa = {
  nama: "gedha",
  energi: 10,
  makan: function (porsi) {
    this.energi = this.energi + porsi;
    console.log(`Selamat makan ${this.nama}`)
  }
}

let mahasiswa2 = {
  nama: "odi",
  energi: 5,
  makan: function (porsi) {
    this.energi = this.energi + porsi;
    console.log(`Selamat makan ${this.nama}`)
  }
}
*/

// ---function declaration---
// const methodMahasiswa = {
//   /*method*/ 
//   makan : function(porsi){
//     this.energi = this.energi + porsi;
//     console.log(`Selamat makan ${this.nama}`)
//   },

//   main : function(jam){
//     this.energi = this.energi - jam;
//     console.log(`Selamat bermain ${this.nama}`)
//   },

//   tidur : function(jam){
//     this.energi = this.energi + jam *2
//     console.log(`Selamat tidur ${this.nama}`)
//   }
// }

// function Mahasiswa (nama, energi){
//   let mahasiswa = Object.create(methodMahasiswa);

//   /*properti*/ 
//   mahasiswa.nama = nama;
//   mahasiswa.energi= energi;



//   return mahasiswa
// }

// /* instansiasi object*/ 
// let gedha = Mahasiswa('gedha', 10);
// let odi = Mahasiswa('odi', 7);

// --- constructor function ---
// function Mahasiswa (nama, energi){
//   /*properti*/ 
//   this.nama = nama;
//   this.energi= energi;

//   /*method*/ 
//   this.makan = function(porsi){
//     this.energi = this.energi + porsi;
//     console.log(`Selamat makan ${this.nama}`)
//   }

//   this.main = function(jam){
//     this.energi = this.energi - jam;
//     console.log(`Selamat bermain ${this.nama}`)
//   }
// }

// /* instansiasi object*/ 
// let gedha = new Mahasiswa('gedha', 10);
// let odi = new Mahasiswa('odi', 7);

// --- Prototype ---
// function Mahasiswa (nama, energi){
//   /*properti*/ 
//   this.nama = nama;
//   this.energi= energi;
// }

// Mahasiswa.prototype.makan = function(porsi){
//   this.energi = this.energi + porsi
//   return `selamat makan ${this.nama}`
// }

// Mahasiswa.prototype.main = function(jam){
//   this.energi = this.energi - jam
//   return `selamat bermain ${this.nama}`
// }

// let gedha = new Mahasiswa('Gedha', 10);


// ---Class Js---
class Mahasiswa {
  constructor(nama, energi){
    this.nama = nama;
    this.energi = energi;
  }

  makan(porsi){
    this.energi = this.energi + porsi
    return `selamat makan ${this.nama}`
  }

  main(jam){
    this.energi = this.energi - jam
    return `selamat bermain ${this.nama}`
  }
}

let gedha = new Mahasiswa('gedha', 10);